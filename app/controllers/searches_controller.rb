class SearchesController < ApplicationController
  before_action :set_search, only: [:show, :edit, :update, :destroy]
  # GET /searches
  # GET /searches.json
  def index
    @searches = Search.all
  end

  # GET /searches/1
  # GET /searches/1.json
  def show

  end

  # GET /searches/new
  def new
    @search = Search.new
  end

  # GET /searches/1/edit
  def edit
  end

  # POST /searches
  # POST /searches.json
  def create

    search_term = search_params['keyword']
    category_term = search_params['category']
    byebug
    if search_term != ""
      response = RestClient.get "https://api.chucknorris.io/jokes/search?query=#{search_term}"
      json = JSON.parse(response)
      if !json['total'].zero?
        @objects = json["result"]
        @objects.paginate(:page => params[:page], :per_page => 30)
        params[:content] = json["result.value"]
        @search = Search.new(search_params)

      else
        render :new
      end
    else
      response = RestClient.get "https://api.chucknorris.io/jokes/random?category=#{category_term}"
      json = JSON.parse(response)
      if !json['categories'].nil?
        @objects_category = json["value"]
      else
        render :new
      end
    end
    end



  # DELETE /searches/1
  # DELETE /searches/1.json
  def destroy
    @search.destroy
    respond_to do |format|
      format.html { redirect_to searches_url, notice: 'Search was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_search
      @search = Search.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def search_params
      params.require(:search).permit(:keyword, :category, :random, :content)
    end


end
