class CreateSearches < ActiveRecord::Migration[5.2]
  def change
    create_table :searches do |t|
      t.string :keyword
      t.string :category
      t.integer :random

      t.timestamps
    end
  end
end
