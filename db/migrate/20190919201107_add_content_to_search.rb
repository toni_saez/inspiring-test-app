class AddContentToSearch < ActiveRecord::Migration[5.2]
  def change
    add_column :searches, :content, :string
  end
end
